function cacheFunction(cb) 
{
    let cache = {};
    if(typeof(cb)=='function')
    {
        return function(user)
        {
            if(typeof(user)!='string' || user.length == 0)
            {
                if(!(typeof(user)=='number'))
                return "'"+user+"'"+" is not valid user name. Please enter string as user name";
                else
                return user+" is not valid user name. Please enter string as user name";
            }
            if(!(cache.hasOwnProperty(user)))
            {
                let res = cb();
                cache[user]=res;
                return res+" for user "+user;
            }
            else
            {
                return "Already Exisit - "+user;
            }
        }
    }
    else
    {
        return function()
        {
            return null;
        }
    }
}
module.exports = cacheFunction;