
function counterFactory()
{
    let counter = 0;

    function increament()
    {
        counter++;
        return counter;
    }
    function decreament()
    {
        counter--;
        return counter;
    }

    return  {in:increament,de:decreament};
}
let result= counterFactory();

module.exports = counterFactory;