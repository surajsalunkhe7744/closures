function limitFunctionCallCount(cb, n) 
{
    if(typeof(cb)=='function' && typeof(n)=='number' && n>0)
    {
    return function()
    {
        let counter=0;
        for(i=0;i<n;i++)
        {
            let res = cb();
            console.log(`Function call ${res}`);
        }
       
        return "";
    }
    }
    else
    {
    return function()
    {
        return null;
    }
}
}

module.exports = limitFunctionCallCount;