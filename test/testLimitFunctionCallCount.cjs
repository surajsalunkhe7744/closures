const testLimitFunctionCallCount = require("../limitFunctionCallCount.cjs")

let counter=0;
function callBackFunction()
{
    return ++counter;
}

console.log(testLimitFunctionCallCount(callBackFunction, 3)());

console.log(testLimitFunctionCallCount(callBackFunction, 0)());

console.log(testLimitFunctionCallCount(callBackFunction)());

console.log(testLimitFunctionCallCount("Hii", 3)());

console.log(testLimitFunctionCallCount()());