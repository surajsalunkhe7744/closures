const testLimitFunctionCallCount = require("../cacheFunction.cjs");

function callBackFunction()
{
    return "Cache created ";
}

let output = testLimitFunctionCallCount(callBackFunction);

console.log(output('user1'));

console.log(output(123));

console.log(output({"test":1}));

console.log(output(""));

console.log(output(''));